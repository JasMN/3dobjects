Model Information:
* title:	Industrial steel table
* source:	https://sketchfab.com/3d-models/industrial-steel-table-8c828ecef936422e932dbfe4c3701c20
* author:	pawuwa (https://sketchfab.com/pawuwa)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Industrial steel table" (https://sketchfab.com/3d-models/industrial-steel-table-8c828ecef936422e932dbfe4c3701c20) by pawuwa (https://sketchfab.com/pawuwa) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)