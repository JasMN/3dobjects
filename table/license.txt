Model Information:
* title:	Wooden tabouret splattered with paint A
* source:	https://sketchfab.com/3d-models/wooden-tabouret-splattered-with-paint-a-168f0b13b6e341c194c84976fc6605dc
* author:	randombug (https://sketchfab.com/randombug)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Wooden tabouret splattered with paint A" (https://sketchfab.com/3d-models/wooden-tabouret-splattered-with-paint-a-168f0b13b6e341c194c84976fc6605dc) by randombug (https://sketchfab.com/randombug) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)